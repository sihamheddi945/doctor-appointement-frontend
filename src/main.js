import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import axios from 'axios'
import VueNotification from "@kugatsu/vuenotification";

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)
Vue.use(VueNotification);

axios.defaults.baseURL = "https://shop2022.pythonanywhere.com"

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
