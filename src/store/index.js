import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    access : '',
    refresh:'',
  },
  mutations: {
    init(state){

      let access = localStorage.getItem("access")
      let refresh = localStorage.getItem("refresh") 
      if (access) {
          state.access = access
          state.refresh = refresh
      } else {
        state.access = ""
        state.refresh = ""
      }

    },
    setAccess(state,access){
      state.access = access
      localStorage.setItem("access",access)
    },
    setRefresh(state,refresh){
      state.refresh = refresh
      localStorage.setItem("refresh",refresh)
    },
    removeToken(state){
      
      localStorage.removeItem("access")
      localStorage.removeItem("refresh")
      state.access = ""
      state.refresh = ""

    }
  },
  actions: {
  },
  modules: {
  }
})
