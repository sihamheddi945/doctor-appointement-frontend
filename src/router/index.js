import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Register from '../views/Register.vue'
import Login from '../views/login.vue'
import Search from '../views/Search.vue'
import Contact from '../views/Contact.vue'
import Dashboard from "../views/doctor/Dashboard.vue"
import Complete from "../views/doctor/Complete.vue";
import axios from "axios"

Vue.use(VueRouter)

function auth(to, from,next) {
  const access = localStorage.getItem("access")

    if (access) {
      console.log("access next ");
      return next()
    } else {
       console.log("access next login");

      return next({name:"login"})

    }
 
  }

function Notauth(to, from,next) {
   const access = localStorage.getItem("access")
    if (!access) {
      console.log("hi there");
      return next()
 
    }
    else {
 
      return next({name:"dashboard"})
 
    }
}

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    beforeEnter:Notauth,
  },
  {
    path: '/complete',
    name: 'Complete',
    component: Complete,
    beforeEnter:auth,
  },
  {
    path:'/dashboard',
    name:'dashboard',
    component:Dashboard,
    beforeEnter:auth,
  },
  {
    path:'/contact/:id',
    name:"Contact",
    component:Contact,
    beforeEnter: Notauth,
  },
  {
   path:"/search/:state/:spec/",
   name:"Search",
   component:Search,
   beforeEnter: Notauth
},
  {
    path:'/register',
    name:'register',
    component:Register,
    beforeEnter: Notauth
  },
  {
    path:'/login',
    name:'login',
    component:Login,
    beforeEnter:  Notauth
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes,
  mode: 'history',

  
})

export default router
